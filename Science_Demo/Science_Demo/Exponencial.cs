﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integral
{
    public class Exponencial
    {
        public double CalculaExponencial(double valorX, double x, double y)
        {
            var calculo1 = (valorX * x);
            var calculo2 = (valorX * y);
            var e = ((Math.Pow(valorX, calculo1) / valorX) - (Math.Pow(valorX, calculo2) / valorX));
            return Math.Round(e, 5);
        }
    }
}
