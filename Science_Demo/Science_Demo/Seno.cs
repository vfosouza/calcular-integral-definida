﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integral
{
    public class Seno
    {
        public double CalcularSeno(double valorX, Int32 valorK, double x, double y)
        {
            x = (x * Math.PI);
            y = (y * Math.PI);
            var calculo = ((valorX * ((Math.Cos(x)) - (Math.Cos(y)))) / (valorK * Math.PI));
            return Math.Round(calculo, 5);
        }

    }
}
