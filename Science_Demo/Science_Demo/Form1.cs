﻿using System;
using System.Windows.Forms;
using Science.Mathematics.VectorCalculus;
using Science.Mathematics;

namespace Integral
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Integration obj = new Integration();

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            if (rdb1.Checked)
            {
                Function.ToLastType<double, double> f = new Function.ToLastType<double, double>(xdx);
                obj.Function = f;
                obj.From = Convert.ToDouble(nupd1.Value);
                obj.To = Convert.ToDouble(nupd2.Value); 
                obj.Compute();
                txtResultado.Text = obj.Result.ToString();
            }
            else if (rdb2.Checked)
            {
                Function.ToLastType<double, double> f = new Function.ToLastType<double, double>(DoisXmais5);
                obj.Function = f;
                obj.From = Convert.ToDouble(nupd3.Value);
                obj.To = Convert.ToDouble(nupd4.Value);
                obj.Compute();
                txtResultado.Text = obj.Result.ToString();
            }
            /*else if (rdb3.Checked)
            {
                Function.ToLastType<double, double> f = new Function.ToLastType<double, double>(x3dx);
                obj.Function = f;
                obj.From = Convert.ToDouble(nupd5.Value);
                obj.To = Convert.ToDouble(nupd6.Value);
                obj.Compute();
                txtResultado.Text = obj.Result.ToString();
            } */
            else if (rdb4.Checked)
            {
                Function.ToLastType<double, double> f = new Function.ToLastType<double, double>(valorX2Mais4XMenosK);
                obj.Function = f;
                obj.From = Convert.ToDouble(nupd7.Value);
                obj.To = Convert.ToDouble(nupd8.Value);
                obj.Compute();
                txtResultado.Text = obj.Result.ToString();
            }
            else if (rdb5.Checked)
            {
                Function.ToLastType<double, double> f = new Function.ToLastType<double, double>(sqrtXCubo);
                obj.Function = f;
                obj.From = Convert.ToDouble(nupd9.Value);
                obj.To = Convert.ToDouble(nupd10.Value);
                obj.Compute();
                txtResultado.Text = obj.Result.ToString();
            }
            else if (rdb6.Checked)
            {
                Cosseno f = new Cosseno();
                var valorIni = Convert.ToDouble(nupd11.Value);
                var valorFim = Convert.ToDouble(nupd12.Value);
                var valorX = Convert.ToDouble(nupd27.Value);
                var valorK = Convert.ToDouble(nupd28.Value);
                var result = f.CalculaCosseno(valorX, valorK, valorIni, valorFim);
                txtResultado.Text = result.ToString();
            }
            else if (rdb7.Checked)
            {
                Seno f = new Seno();
                var valorIni = Convert.ToDouble(nupd13.Value);
                var valorFim = Convert.ToDouble(nupd14.Value);
                var valorX = Convert.ToDouble(nupd29.Value);
                var valorK = Convert.ToInt32(nupd30.Value);
                var result = f.CalcularSeno(valorX, valorK, valorFim, valorIni);
                txtResultado.Text = result.ToString();

            }
            else if (rdb8.Checked)
            {
                Exponencial f = new Exponencial();
                var valorIni = Convert.ToDouble(nupd15.Value);
                var valorFim = Convert.ToDouble(nupd16.Value);
                var valorX = Convert.ToDouble(nupd31.Value);
                var result = f.CalculaExponencial(valorX, valorIni, valorFim);
                txtResultado.Text = result.ToString();
            }
            else if (rdb9.Checked)
            {
                Function.ToLastType<double, double> f = new Function.ToLastType<double, double>(xdx_1);
                obj.Function = f;
                obj.From = Convert.ToDouble(nupd19.Value);
                obj.To = Convert.ToDouble(nupd20.Value);
                obj.Compute();
                txtResultado.Text = obj.Result.ToString();
            }
            else
            {
                MessageBox.Show("Selecione uma integral para cálculo.", "Selecione uma integral", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private double xdx(double x)
        {
            var valorX = Convert.ToDouble(nupd17.Value);
<<<<<<< HEAD
            return (valorX / 1);
=======
            return (valorX * x);
>>>>>>> f8e903e5805a79056e62f901890a905e16a9efc0
        }

        private double xdx_1(double x)
        {
            var valorX = Convert.ToDouble(nupd18.Value);
            return ((valorX / 2) * x);
        }

        private double DoisXmais5(double x)
        {
            var valorX = Convert.ToDouble(nupd21.Value);
            var valorK = Convert.ToDouble(nupd22.Value);
            return ((valorX * x) + valorK);
        }

        private double x2dx(double x)
        {
            return x*x;
        }
        private double x3dx(double x)
        {
            return x*x*x;
        }
        private double valorX2Mais4XMenosK(double x)
        {
            var valorX = Convert.ToDouble(nupd23.Value);
            var valorXX = Convert.ToDouble(nupd24.Value);
            var valorK = Convert.ToDouble(nupd25.Value);
            return ((- valorX *(x * x)) + ((valorXX * x)) - valorK);
        }
        private double sqrtXCubo(double x)
        {
            var valorX = Convert.ToDouble(nupd26.Value);
            return Math.Sqrt((valorX * (x * x * x)));
        }
    }
}
