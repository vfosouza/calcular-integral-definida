﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integral
{
    public class Cosseno
    {
        public double CalculaCosseno(double valorX, double valorK, double x, double y)
        {
            x = (x * Math.PI);
            y = (y * Math.PI);
            var Calculo = ((valorX * ((Math.Sin(x)) - (Math.Sin(y)))) / (valorK * Math.PI));
            return Math.Round(Calculo, 5);
        }
    }
}
